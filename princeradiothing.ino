#include <esp_now.h>
#include <WiFi.h>

long lastSent;
int sendInterval = 2000;

uint8_t broadcastAddress[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};  //ff
//uint8_t broadcastAddress[] = {0xE8, 0x9F, 0x6D, 0xA7, 0xD9, 0xF0};  //1
//uint8_t broadcastAddress[] = {0xE8, 0x9F, 0x6D, 0xA8, 0x15, 0x98};  //2

typedef struct struct_message {
  char a[64];
  int b = 0;
} struct_message;

struct_message myData;

esp_now_peer_info_t peerInfo;

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");

  if (status == ESP_NOW_SEND_SUCCESS) {
    Serial.println("Delivery Success");
    digitalWrite(2, !digitalRead(2));
  }

}
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));

  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("Char: ");
  Serial.println(myData.a);
  PrintString();
  Serial.print("Int: ");
  Serial.println(myData.b);
  Serial.println();
  digitalWrite(2, !digitalRead(2));
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  keyboardsetup();
  displaysetup();
  pinMode(2, OUTPUT);
  // Init Serial Monitor

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  esp_now_register_recv_cb(OnDataRecv);

  // Register peer
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  // Add peer
  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
    Serial.println("Failed to add peer");
    return;
  }

}

void loop() {
  // put your main code here, to run repeatedly:
  //Serial.println("hello world");
  keyboardloop();



  delay(1);
}


void SendData() {
  esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));

  if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else {
    Serial.println("Error sending the data");
  }
}



void TransmitSendBuffer(){
  for (int i = 0; i < 64; i++){
     myData.a[i] = GetMessage(i);
  }
  SendData();
}
