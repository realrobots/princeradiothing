#include <Wire.h>
#include <MCP23017.h>




MCP23017 mcp1 = MCP23017(32);

MCP23017 mcp2 = MCP23017(36);
//                   0    1    2    3    4    5    6   7     8    9    10   11   12   13   14   15   16   17   18  19     20   21  22  23   24    25    26   27  28   29   30   31
char keyLabels[] = {'Q', 'A', '0', '0', 'W', 'S', 'Z', 'E', 'G', 'V', 'T', 'C', 'F', 'R', 'X', 'D', 'Y', 'H', 'B', 'U', 'J', 'N', 'I', 'K', '0', '0', 'P', '.', 'L', 'O', 'M', ' '};
char keyLabelsFunc[] = {'1', '0', '0', '0', '2', '0', '0', '3', '0', '0', '5', '0', '0', '4', '0', '0', '6', '0', '0', '7', '0', '0', '8', '0', '0', '0', '0', '.', '0', '9', '0', '0'};

#define KEY_SHIFT 2
#define KEY_FUNC  3
#define KEY_DEL   24
#define KEY_ENTER 25

bool funcHeld = false;

uint8_t buttonState1A = 255;
uint8_t buttonState1B = 255;
uint8_t buttonState2A = 255;
uint8_t buttonState2B = 255;

uint8_t prevButtonState1A = 255;
uint8_t prevButtonState1B = 255;
uint8_t prevButtonState2A = 255;
uint8_t prevButtonState2B = 255;

char GetKeyLabel(int idx, bool func = false) {
  if (func) {
    return keyLabelsFunc[idx];
  } else {
    return keyLabels[idx];
  }
}

void keyboardsetup() {
  Wire.begin();
  mcp1.init();
  mcp1.portMode(MCP23017Port::A, 0b11111111);          //Port A as output
  mcp1.portMode(MCP23017Port::B, 0b11111111); //Port B as input

  mcp2.init();
  mcp2.portMode(MCP23017Port::A, 0b11111111);          //Port A as output
  mcp2.portMode(MCP23017Port::B, 0b11111111);






}


void keyboardloop() {
  buttonState1A = mcp1.readPort(MCP23017Port::A);
  buttonState1B = mcp1.readPort(MCP23017Port::B);
  buttonState2A = mcp2.readPort(MCP23017Port::A);
  buttonState2B = mcp2.readPort(MCP23017Port::B);

  funcHeld = !bitRead(buttonState1A, KEY_FUNC);
  
  

  for (int i = 0; i < 8; i++) {
    if (bitRead(buttonState1A, i) == 0) {
      if (bitRead(prevButtonState1A, i) == 1) {
        onKeyPressed(0 + i);
      } else {
        onKey(0 + i);
      }
    } else {
      if (bitRead(prevButtonState1A, i) == 0) {
        onKeyReleased(0 + i);
      }
    }
  }
  for (int i = 0; i < 8; i++) {
    if (bitRead(buttonState1B, i) == 0) {
      if (bitRead(prevButtonState1B, i) == 1) {
        onKeyPressed(8 + i);
      } else {
        onKey(8 + i);
      }
    } else {
      if (bitRead(prevButtonState1B, i) == 0) {
        onKeyReleased(8 + i);
      }
    }
  }


  for (int i = 0; i < 8; i++) {
    if (bitRead(buttonState2A, i) == 0) {
      if (bitRead(prevButtonState2A, i) == 1) {
        onKeyPressed(16 + i);
      } else {
        onKey(16 + i);
      }
    } else {
      if (bitRead(prevButtonState2A, i) == 0) {
        onKeyReleased(16 + i);
      }
    }
  }
  for (int i = 0; i < 8; i++) {
    if (bitRead(buttonState2B, i) == 0) {
      if (bitRead(prevButtonState2B, i) == 1) {
        onKeyPressed(24 + i);

      } else {
        onKey(24 + i);
      }
    } else {
      if (bitRead(prevButtonState2B, i) == 0) {
        onKeyReleased(24 + i);
      }
    }
  }

  prevButtonState1A = buttonState1A;
  prevButtonState1B = buttonState1B;
  prevButtonState2A = buttonState2A;
  prevButtonState2B = buttonState2B;
}

void onKeyReleased(int key) {
  //Serial.print("keyReleased ");
  //Serial.println(key);
}

void onKeyPressed(int key) {
  //  Serial.print("keyPressed ");
  //  Serial.print(key);
  //  Serial.print("\t");
  //  Serial.println(keyLabels[key]);

  InterpretKeyPress(key);
}

void InterpretKeyPress(int key) {
  Serial.println(key);
  if (key == KEY_ENTER) {
    PrintMessageBuffer();
    ClearSendBuffer();    
    TransmitSendBuffer();
    
    
    ClearMessageBuffer();
    ClearScreen();
  }
  else if (key == KEY_DEL || key == KEY_SHIFT || key == KEY_FUNC) {
    Serial.println("noPressed");
  }
  else 
  {
    DrawChar(GetKeyLabel(key, funcHeld));
    Serial.print(funcHeld);
    Serial.println(" letterPressed");
  }


}

void onKey(int key) {
  //Serial.print("key ");
  //Serial.println(key);
}



//void printkeyboard() {
//  for (int i = 0; i < 8; i++) {
//
//    Serial.print(bitRead(currentA, i));
//  }
//
//  for (int i = 0; i < 8; i++) {
//
//    Serial.print(bitRead(currentB, i));
//  }
//
//  for (int i = 0; i < 8; i++) {
//
//    Serial.print(bitRead(currentA2, i));
//  }
//
//  for (int i = 0; i < 8; i++) {
//
//    Serial.print(bitRead(currentB2, i));
//  }
//
//  Serial.println();
//  delay(10);
//}
