#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define OLED_RESET 4
#define SCREEN_ADDRESS 60
int space = 0;
int spaceY = 0;
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

char message[64];
int messageLength = 0;

char GetMessage(int idx){
  return message[idx];
}

void ClearSendBuffer() {
 
//  for (int i = 0; i < 64; i++){
//     message[i] = 0;
//  }
// messageLength = 0;
}

void displaysetup()
{
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ; // Don't proceed, loop forever
  }
  display.display();
  delay(20);
  display.clearDisplay();
}

void ClearScreen() {
  Serial.println("clearScreen");
  display.clearDisplay();
  display.display();
  space = 0;
  spaceY = 0;
}

void PrintString(){
  //ClearScreen();
  //DrawChar('b');
  //display.display();
  //Serial.println("TRYING TO SHOW INCOMING MESSAGE");
}

void PrintMessageBuffer() {
  for (int i = 0; i < messageLength; i++) {
    Serial.print(message[i]);
    Serial.print(",");
  }
  Serial.println();
}
void ClearMessageBuffer() {
  messageLength = 0;
}

void DrawChar(char c) {
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(space, spaceY);
  display.cp437(true);
  //Serial.println(whatkey);
  //display.write(whatkey);

  message[messageLength] = c;
  messageLength += 1;

  space = space + 12;
  display.print(c);

  if (space > 110) {
    space = 0;
    spaceY = spaceY + 20;
  }

  display.display();
  delay(20);
}
